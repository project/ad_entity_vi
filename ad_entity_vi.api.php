<?php

/**
 * @file
 * Documentation for the Advertising Entity: Video Intelligence module.
 *
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Alter the keywords before Video Intelligence ad render.
 *
 * @param array $keywords
 *   Keywords array.
 */
function hook_ad_entity_vi_keywords_alter(array &$keywords) {

}
